#include "AllocDefs.hpp"

AllocDefinition *AllocDefManager::getAllocDef(Function *f) {
    for (auto it = allocDefs.begin(); it != allocDefs.end(); it++) {
        if (it->name.compare(f->getName().str()) == 0) {
            return &(*it);
        }
    }
    return NULL;
}

void AllocDefManager::loadAllocDefs() {
    ifstream file(getAllocInPath());
    string line;
    if (!file.is_open()) {
        cerr << "Error opening allocation definitions. Falling back to standard m/calloc.\n" << endl;

        AllocDefinition mallocDef;
        mallocDef.name = "malloc";
        mallocDef.sizeIdx = 0;
        mallocDef.numIdx = -1;
        mallocDef.addrIdx = -1;
        mallocDef.isDealloc = false;
        allocDefs.push_back(mallocDef);

        AllocDefinition callocDef;
        callocDef.name = "calloc";
        callocDef.sizeIdx = 1;
        callocDef.numIdx = 0;
        callocDef.addrIdx = -1;
        callocDef.isDealloc = false;
        allocDefs.push_back(callocDef);

        AllocDefinition freeDef;
        freeDef.name = "free";
        freeDef.sizeIdx = -1;
        freeDef.numIdx = -1;
        freeDef.addrIdx = 0;
        freeDef.isDealloc = true;
        allocDefs.push_back(freeDef);
    }
    while (getline(file, line)) {
        // trim perhaps?
        if (line[0] == '#') continue;
        if (line.length() <= 2) continue;

        AllocDefinition allocDef;
        istringstream iss(line);
        if (!(iss >> allocDef.name >> allocDef.numIdx >> allocDef.sizeIdx >> allocDef.addrIdx)) {
            cerr << "Error parsing allocation input, exiting\n" << endl;
            cerr << "line: " << line << endl;
            exit(-1);
        }
        if (allocDef.sizeIdx == -1) {
            allocDef.isDealloc = true;
        } else {
            allocDef.isDealloc = false;
        }
        cerr << "Adding allocdef " << allocDef.name << endl;
        allocDefs.push_back(allocDef);
    }
}
string AllocDefManager::getAllocInPath() {
    const char * val = ::getenv("ALLOC_IN");
    if ((val == 0) || (strcmp(val,"") == 0)) {
        cerr << "ALLOC_IN path not set, defaulting to ./alloc.in" << endl;
        return "./alloc.in";
    }
    else {
        string s = val;
        s += "/alloc.in";
        cerr << "alloc.in at " << s << endl;
        return s;
    }
}
