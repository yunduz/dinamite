#!/bin/bash

if [ -z $1 ]; then
    TEST=main
else
    TEST=$1
fi

if [[ "$OSTYPE" == 'darwin'* ]]; then
    LIBNAME='AccessInstrument.dylib'
else
    LIBNAME='AccessInstrument.so'
fi

if [ -z $CLANG ]; then
    CLANG='clang'
fi


#COMPILER_PATH=/usr/lib/gcc/x86_64-amazon-linux/4.8.3  DIN_FILTERS="function_filter.json" $CLANG -O3 -g -v -Xclang -load -Xclang \
#    ../../Release+Asserts/lib/${LIBNAME} \
#    tests/${TEST}.c -L./library/ -L/usr/lib/gcc/x86_64-amazon-linux/4.8.3 -o ${TEST}

DIN_FILTERS="function_filter.json" $CLANG -O3 -g -v -Xclang -load -Xclang \
     ../../Release+Asserts/lib/${LIBNAME} \
    tests/${TEST}.c -L./library/ -linstrumentation -o ${TEST}

