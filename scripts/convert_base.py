#!/usr/bin/python
import sys
import json

def load_map(filename):
    with open(filename, "r") as f:
        invmap = json.load(f)
        map = dict()
        for k in invmap:
            map[invmap[k]] = k
        return map

def load_mappings(dir):
    mapping = dict();
    mapping["fn"] = load_map("%s/map_functions.json" % dir)
    mapping["src"] = load_map("%s/map_sources.json" % dir)
    mapping["var"] = load_map("%s/map_variables.json" % dir)
    mapping["type"] = load_map("%s/map_types.json" % dir)
    return mapping;



def main():
    filename = sys.argv[1]
    map_dir = sys.argv[2]
    print "%s %s" % (filename, map_dir)
    m = load_mappings(map_dir)
    print json.dumps(m, indent=2)

if __name__=="__main__":
    main()
