#ifndef INSTRUMENTATION_H
#define INSTRUMENTATION_H

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#include "dinamite_time.h"


#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)

const char *filename = "tp_text.trace";
static FILE *out = NULL;

static struct timeval now_time;

void logInit(int functionId) {

	if (unlikely(out == NULL)) {
		printf("Opening log file %s...\n", filename);
		out = fopen(filename,"w");
	}
}

void logExit(int functionId) {

	if (out != NULL) {
		printf("Closing log file %s...\n", filename);
		fclose(out);
		out = NULL;
	}
}

void logFnBegin(int functionId) {

	uint64_t time = dinamite_time_nanoseconds();

	fprintf(out, "FN_START_EVENT FunctionId: %d nanos: %lu\n", functionId,
		time);
	fflush(out);
}

void logFnEnd(int functionId) {

	uint64_t time = dinamite_time_nanoseconds();

	fprintf(out, "FN_END_EVENT FunctionId: %d nanos: %lu\n", functionId,
		time);
	fflush(out);
}

void logAlloc(void *addr, uint64_t size, uint64_t num, int type, int file,
	      int line, int col) {
}

void logAccessPtr(void *ptr, void *value, int type, int file, int line, int col,
		  int typeId, int varId) {

	fprintf(out, "logAccessPtr: %s\n", (char*)value);
	fflush(out);

}

void logAccessStaticString(void *ptr, void *value, int type, int file, int line, int col, int typeId, int varId) {
}


void logAccessI8(void *ptr, uint8_t value, int type, int file, int line,
		 int col, int typeId, int varId) {

	fprintf(out, "%p %u %c %d %d %d %d %d\n", ptr, value, type, file, line,
		col, typeId, varId);
	fflush(out);
}

void logAccessI16(void *ptr, uint16_t value, int type, int file, int line, int col, int typeId, int varId) {
	fprintf(out, "%p %hu %c %d %d %d %d %d\n", ptr, value, type, file, line, col, typeId, varId);
    fflush(out);

	/*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

void logAccessI32(void *ptr, uint32_t value, int type, int file, int line, int col, int typeId, int varId) {
	fprintf(out, "%d\n", value);
    fflush(out);

	/*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

void logAccessI64(void *ptr, uint64_t value, int type, int file, int line, int col, int typeId, int varId) {
	fprintf(out, "%p %lu %c %d %d %d %d %d\n", ptr, value, type, file, line, col, typeId, varId);
    fflush(out);

	/*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

/* =============================
 These don't exist: */

void logAccessF8(void *ptr, uint8_t value, int type, int file, int line, int col, int typeId, int varId) {
	fprintf(out, "%p %c %c %d %d %d %d %d\n", ptr, value, type, file, line, col, typeId, varId);
    fflush(out);

	/*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

void logAccessF16(void *ptr, uint16_t value, int type, int file, int line, int col, int typeId, int varId) {
	fprintf(out, "%p %hu %c %d %d %d %d %d\n", ptr, value, type, file, line, col, typeId, varId);
    fflush(out);

	/*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

/* ============================= */

void logAccessF32(void *ptr, float value, int type, int file, int line, int col, int typeId, int varId) {
	fprintf(out, "%p %f %c %d %d %d %d %d\n", ptr, value, type, file, line, col, typeId, varId);
    fflush(out);

	/*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

void logAccessF64(void *ptr, double value, int type, int file, int line, int col, int typeId, int varId) {
	fprintf(out, "%p %lf %c %d %d %d %d %d\n", ptr, value, type, file, line, col, typeId, varId);
    fflush(out);

	/*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}
#endif
