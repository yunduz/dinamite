#ifndef INSTRUMENTATION_H
#define INSTRUMENTATION_H

#include <errno.h>
#include <limits.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/syscall.h>

#include "binaryinstrumentation.h"
#include "dinamite_time.h"
#include "perthreadlogging.h"

#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)


static inline void
fillFnLog(fnlog *fnl, char fn_event_type, int functionId, pid_t tid) {
    fnl->fn_event_type = fn_event_type;
    fnl->function_id = functionId;
    fnl->fn_timestamp = (uint64_t) dinamite_time_nanoseconds();
    fnl->thread_id = tid;
}

void fillAccessLog(accesslog *acl, void *ptr, char value_type,
           value_store value, int type, int file, int line,
                   int col, int typeId, int varId, pid_t tid) {
    acl->thread_id = tid;
    acl->ptr = ptr;
    acl->value_type = value_type;
    acl->value = value;
    acl->type = type;
    acl->file = file;
    acl->line = line;
    acl->col = col;
    acl->typeId = typeId;
    acl->varId = varId;
    acl->ac_timestamp = (uint64_t) dinamite_time_nanoseconds();
}

void fillAllocLog(alloclog *all, void *addr, uint64_t size, uint64_t num,
                  int type, int file, int line, int col, pid_t tid) {
    all->thread_id = tid;
    all->addr = addr;
    all->size = size;
    all->num = num;
    all->type = type;
    all->file = file;
    all->line = line;
    all->col = col;
    all->al_timestamp = (uint64_t) dinamite_time_nanoseconds();
}

void logFnBegin(int functionId) {
    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_FN;
    fillFnLog(&(le->entry.fn), FN_BEGIN, functionId, tid);
}

void logFnEnd(int functionId) {
    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_FN;
    fillFnLog(&(le->entry.fn), FN_END, functionId, tid);
}

void logAlloc(void *addr, uint64_t size, uint64_t num, int type, int file,
              int line, int col) {
    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_ALLOC;
    fillAllocLog(&(le->entry.alloc),
                 addr, size, num, type, file, line, col, tid);
}

void logAccessPtr(void *ptr, void *value, int type, int file, int line, int col,
          int typeId, int varId) {
    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_ACCESS;
    value_store vs;
    vs.ptr = value;
    fillAccessLog(&(le->entry.access), ptr, PTR, vs, type, file, line, col,
                  typeId, varId, tid);
}

/* This function logs an access when we are sure that what we are acccessing is
 * a null-terminated string. A typical use-case is when we print a string passed
 * as an argument to the tracepoint function. Using this function when we are
 * not sure whether the address points to a null-terminated string is unsafe,
 * because we may crash when we try to print it later.
 *
 * Another crucial assumption we are making is that the strings being accessed
 * are static. Here is the reason: To avoid the runtime overhead associated
 * with string printing, this function simply stores the pointer when called,
 * and at the very end of the program goes over the pointers and prints them.
 * Here we are assuming that the pointers accessed earlier in the program are
 * still valid at the end of the program and that they are still pointing to the
 * same values as they did when they were actually accessed. This will be true
 * for static strings, but may not be true for dynamic strings. So this function
 * is not safe to use with dynamically allocated strings.
 */
void logAccessStaticString(void *ptr, void *value, int type, int file, int line,
               int col, int typeId, int varId) {

    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_ACCESS;
    value_store vs;
    vs.ptr = value;
    fillAccessLog(&(le->entry.access), ptr, PTR, vs, type, file, line, col,
                  typeId, varId, tid);
}

void logAccessI8(void *ptr, uint8_t value, int type, int file, int line,
         int col, int typeId, int varId) {
    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_ACCESS;
    value_store vs;
    vs.i8 = value;
    fillAccessLog(&(le->entry.access), ptr, I8, vs, type, file, line, col,
                  typeId, varId, tid);
}

void logAccessI16(void *ptr, uint16_t value, int type, int file, int line,
          int col, int typeId, int varId) {
    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_ACCESS;
    value_store vs;
    vs.i16 = value;
    fillAccessLog(&(le->entry.access), ptr, I16, vs, type, file, line, col,
                  typeId, varId, tid);
}

void logAccessI32(void *ptr, uint32_t value, int type, int file, int line,
          int col, int typeId, int varId) {
    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_ACCESS;
    value_store vs;
    vs.i32 = value;
    fillAccessLog(&(le->entry.access), ptr, I32, vs, type, file, line, col,
                  typeId, varId, tid);
}

void logAccessI64(void *ptr, uint64_t value, int type, int file, int line,
          int col, int typeId, int varId) {
    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_ACCESS;
    value_store vs;
    vs.i64 = value;
    fillAccessLog(&(le->entry.access), ptr, I64, vs, type, file, line, col,
                  typeId, varId, tid);
}

/* =============================
   These don't exist: */

void logAccessF8(void *ptr, uint8_t value, int type, int file, int line,
         int col, int typeId, int varId) {
}

void logAccessF16(void *ptr, uint16_t value, int type, int file, int line,
          int col, int typeId, int varId) {

}

/* ============================= */

void logAccessF32(void *ptr, float value, int type, int file, int line, int col,
          int typeId, int varId) {
    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_ACCESS;
    value_store vs;
    vs.f32 = value;
    fillAccessLog(&(le->entry.access), ptr, F32, vs, type, file, line, col,
                  typeId, varId, tid);
}

void logAccessF64(void *ptr, double value, int type, int file, int line,
          int col, int typeId, int varId) {
    pid_t tid;
    logentry *le = __dinamite_getBufPtrMaybeFlush(&tid);
    if (le == NULL)
        return;

    le->entry_type = LOG_ACCESS;
    value_store vs;
    vs.f64 = value;
    fillAccessLog(&(le->entry.access), ptr, F64, vs, type, file, line, col,
                  typeId, varId, tid);
}
#endif
